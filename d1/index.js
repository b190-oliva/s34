/* 	npm init
	- package.json repo
	- package.json tracks the version of our application
	npm install express
	- express will be listed as a dependency
	- installing any dependencies using npm will result into creating "node modules" folder and package-lock.json
	- node_modules directory should be left on the local repository
	- node modules is also where the dependecies needed files are stored.

	".gitignore" file - to ignore the modules to be uploaded on gitlab

	"npm install"
	- this command is used when there are available dependencies inside our "package.json" but are not yet installed inside the repo/project. This is useful when trying to clone a repo from a git repository to our local repository
*/

// we need now the express module since in this dependency, it has already built in codes that will allow the creation  of a server
const express = require("express");
// express() - creation of express server
const app = express();
// port variable
const port = 3000;
// app.use lets the server to handle the data from request
// app.use(express.json()); - allows the app to read and handle json data types
//method used from express middlewares
// middlewares - software that provide common services and capabilities for the application
app.use(express.json());
// code below allows the server to read the data from forms
//by default information received from the url can only be received as a string or an array
// but using extended: true - allows us to receive information from other data types such as objects that we will use
app.use(express.urlencoded({extended: true}));

// SECTION - routes
// express has methods corresponding to each HTTP methods
// the routes below expects to receive a GET request at the base URI "/"
// app.get("/",(req,res)=>{
// 	res.send("Hello world!");
// });

// app.get("/hello",(req,res)=>{
// 	res.send("Hello from /hello endpoint");
// });

// // POST method route
// // this routes expects to receive a POST request at the URI "/hello"
// app.post("/hello",(req,res)=>{
// 	res.send(`Hello from the other side ${req.body.firstName} ${req.body.lastName}!`);
// });
let users = [];

app.post("/signup", (req,res) =>{
	if(req.body.firstName === "" && req.body.lastName === ""){
		res.send(`Please enter your first and last name!`);
	}
	else{
		users.push(req.body);
		res.send(`User ${req.body.firstName} ${req.body.lastName} succesfully signed up!`);
		// console.log(typeof req.body);
	}
});

// app.put("/change-name",(req,res)=>{
// 	let message;
// 	for(let counter = 0; counter<users.length; counter++){
// 		if(req.body.firstName == users[counter].firstName){
// 			users[counter].lastName = req.body.lastName;
// 			message = `user ${req.body.firstName} succesfully changed last name into ${req.body.lastName}`;
// 			// console.log(req.body);
// 			break;
// 		}
// 		else{
// 			message = "User doesn't exist!"
// 		}
// 	}
// 	res.send(message);
// })

// ====== activity ======

// - /home using GET
app.get("/home", (req,res)=>{
	res.send(`Welcome to the homepage`);
});

// GET request to access /users however need to sign up first to fill the firstName and lastName

app.get("/users",(req,res)=>{
	res.send(users);
});

// DELETE route

app.delete("/delete-user", (req,res) =>{
	res.send(`User ${req.body.firstName} has been deleted.`);
});

app.listen(port, () => console.log(`Server running at port: ${port}`));

